<?php

namespace Core;

use Core\Helper\ClassHelper;
use PDO;
use App\Config;

/**
 * Class Model
 * @package Core
 */
abstract class Model
{
    /**
     * @var PDO
     */
    static $db = null;
    public $data = [];
    public $errors = [];

    /**
     * Get the PDO database connection
     *
     * @return mixed
     */
    protected static function getDB()
    {
        if (self::$db === null) {
            $dsn = 'mysql:host=' . Config::DB_HOST . ';dbname=' . Config::DB_NAME . ';charset=utf8';
            self::$db = new PDO($dsn, Config::DB_USER, Config::DB_PASSWORD);

            self::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }

        return self::$db;
    }

    /**
     * @return string
     */
    public function className()
    {
        return ClassHelper::getClassName($this);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [];
    }

    /**
     * @param array $post
     *
     * @return bool
     */
    public function load($post)
    {
        if (!isset($post[$this->className()])) {
            return false;
        }
        $this->data = $post[$this->className()];
        return true;
    }

    /**
     * @param null|string $name
     *
     * @return array|mixed|null
     */
    public function getLabels($name = null)
    {
        if ($name) {
            return isset($this->attributeLabels()[$name]) ? $this->attributeLabels()[$name] : $name;
        }
        return $this->attributeLabels();
    }

    /**
     *  Validate post data
     */
    public function validate()
    {
        foreach ($this->data AS $k => $v) {
            if (!property_exists($this, $k)) {
                unset($this->data[$k]);
            }
        }
    }

    /**
     * @param string $message
     * @param string $field
     */
    public function addError($message, $field = '')
    {
        if (!$field) {
            $this->errors[] = $message;
        } else {
            $this->errors[$field] = $message;
        }
    }

    /**
     * @return bool
     */
    public function hasError()
    {
        return sizeof($this->errors) > 0 ? true : false;
    }

    /**
     * @param string $field
     *
     * @return bool
     */
    public function hasErrorField($field)
    {
        return isset($this->errors[$field]) && $this->errors[$field] ? true : false;
    }

    /**
     * @param string $field
     *
     * @return mixed|null
     */
    public function getError($field)
    {
        return isset($this->errors[$field]) ? $this->errors[$field] : null;
    }

    /**
     * @param bool $inString
     *
     * @return array|string
     */
    public function getErrors($inString = false)
    {
        if ($inString) {
            return '<p>' . join('</p><p>', $this->errors) . '</p>';
        }
        return $this->errors;
    }

    /**
     * @param string $tableName
     * @param array $data
     */
    public function insert(string $tableName, array $fields, array $data)
    {
        $sql = 'INSERT INTO ' . $tableName . ' (' . $this->prepareData($fields) . ')' . ' VALUES (' . $this->prepareData($data) . ')';
        var_dump($sql);
        die;
        self::$db->query($sql)->execute();
    }

    /**
     * @param array $data
     * @return string
     */
    private function prepareData(array $data) : string
    {
        $preparedString = null;
        $iterator = 0;

        foreach ($data as $value) {
            if ($iterator === 0) {

                $preparedString = $value;
            } elseif ($value !== null) {
                $preparedString = $preparedString . ', ' . $value;
            }
            $iterator++;
        }
        return ltrim($preparedString, ', ');
    }
}
