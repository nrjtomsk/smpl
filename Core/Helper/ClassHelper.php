<?php
/**
 * Created by PhpStorm.
 * User: nrjto
 * Date: 07.10.2016
 * Time: 19:50
 */

namespace Core\Helper;


/**
 * Class ClassHelper
 * @package system\helper
 */
class ClassHelper
{
    /**
     * @param $class
     *
     * @return string
     */
    public static function getClassName($class){
        return join('', array_slice(explode('\\', get_class($class)), -1));
    }
}