<?php

namespace Core;

use Core\Helper\ClassHelper;

/**
 * Class Controller
 * @package Core
 */
abstract class Controller
{
    public $view;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        $name = ClassHelper::getClassName($this);
        $this->view = new View($name);
    }

    public function refresh()
    {
        header("Refresh:0");
    }
}
