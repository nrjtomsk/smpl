<?php

namespace App\Service;

use App\Config;
use Eventviva\ImageResize;
use upload;

class ImageService
{
    /** @var  ImageResize */
    public $imageResize;

    /**
     * @param array $file
     * @return array
     * @throws \Exception
     */
    public function uploadImage(array $file)
    {
        $handle = new upload($file['image']);
        if ($handle->uploaded) {
            $handle->process('../public/img/');
            if ($handle->processed) {
                $this->resizeImage($handle->file_dst_path, $handle->file_dst_name);
                return [
                    'name' => $handle->file_dst_name,
                    'path' => $handle->file_dst_path
                ];
            } else {
                throw new \Exception($handle->error);
            }
        } else {
            throw new \Exception($handle->error);
        }
    }

    /**
     * @param string $path
     * @param string $name
     */
    private function resizeImage(string $path, string $name)
    {
        $filename = $path . $name;

        $this->imageResize = new ImageResize($filename);

        if ($this->imageResize->getDestWidth() > 320) {
            $this->imageResize->resize(320, $this->imageResize->getDestHeight());
        }

        if ($this->imageResize->getDestHeight() > 240) {
            $this->imageResize->resize($this->imageResize->getDestWidth(), 240);
        }

        $this->imageResize->save($filename);
    }

}