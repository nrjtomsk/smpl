<?php
/**
 * Created by PhpStorm.
 * User: nrjto
 * Date: 07.10.2016
 * Time: 21:19
 */

namespace App\Service;


class FeedbackService
{
    /**
     * Prepare data for view
     * @param array $data
     * @return array
     */
    public static function prepareDataView(array $data): array
    {
        foreach ($data as &$feedback)
        {
            if (isset($feedback['changed'])) {
                if ($feedback['changed'] == 1) {
                    $feedback['changed'] = 'Изменнено администратором';
                } elseif ($feedback['changed'] == 0) {
                    $feedback['changed'] = '';
                }
            }

        }

        unset($feedback);

        return $data;
    }

    /**
     * Prepare data to DB
     *
     * @param array $data
     * @return array
     */
    public static function prepareDataDB(array $data): array
    {

        $data['username'] = '\'' . $data['username'] . '\'';
        $data['email'] = '\'' . $data['email'] . '\'';
        $data['text'] = '\'' . $data['text'] . '\'';

        return $data;
    }
}