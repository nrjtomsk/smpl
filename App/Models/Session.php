<?php
/**
 * Created by PhpStorm.
 * User: nrjto
 * Date: 09.10.2016
 * Time: 21:34
 */

namespace App\Models;

use PDO;
use Core\Model;

class Session extends Model
{
    /** @var PDO */
    public static $db;

    public $fields = [
        'user', 'deleted'
    ];

    /**
     * @return string
     */
    public function tableName()
    {
        return "session";
    }

    public function __construct()
    {
        self::$db = static::getDB();
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getNotExpiredSessionByUserId($id = null)
    {
        if ($id !== null && !empty($id)) {
            $stmt = self::$db->prepare('SELECT FROM :tablename WHERE `user` = :user_id AND `deleted` = NULL');
            $stmt->bindParam(':user_id', $id);
            return $stmt->fetch();
        } else {
            return null;
        }
    }

    public function createSession(int $userId): int
    {
        $data = [
            'user' => $userId,
            'deleted' => NULL
        ];

        parent::insert($this->tableName(), $this->fields, $data);
        return self::$db->lastInsertId();
    }
}