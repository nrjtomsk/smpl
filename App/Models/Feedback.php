<?php
/**
 * Created by PhpStorm.
 * User: nrjto
 * Date: 07.10.2016
 * Time: 16:10
 */

namespace App\Models;

use PDO;
use Core\Model;

/**
 * Class Feedback
 * @package App\Models
 */
class Feedback extends Model
{
    public static $db;

    public $fields = [
        'username', 'email', 'text', 'image',
    ];

    public function __construct()
    {
        self::$db = static::getDB();
    }

    /**
     * @return string
     */
    public function tableName()
    {
        return "feedback";
    }

    /**
     * Get all the users as an associative array
     *
     * @return array
     */
    public static function getAll()
    {
        $stmt = self::$db->query('SELECT * FROM feedback');
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @return bool
     */
    public function validate()
    {
        if (!$this->data) {
            $this->addError('Нет данных', '');
        }
        if (!isset($this->data['username'])) {
            $this->addError('Нужно заполнить ' . $this->getLabels('name'), 'name');
        }

        if (!isset($this->data['email'])) {
            $this->addError('Нужно заполнить ' . $this->getLabels('email'), 'email');
        } elseif (!filter_var($this->data['email'], FILTER_VALIDATE_EMAIL)) {
            $this->addError('Неверный ' . $this->getLabels('email'), 'email');
        }

        if (!isset($this->data['text'])) {
            $this->addError('Нужно заполнить поле ' . $this->getLabels('text'), 'text');
        }
        parent::validate();
        return true;
    }

    /**
     * @param string $tableName
     * @param array $fields
     * @param array $data
     */
    public function insert(string $tableName, array $fields, array $data)
    {
        if (isset($data['image'])) {
            parent::insert($this->tableName(), $this->fields, $data);
        } else {
            $fields = ['username', 'email', 'text'];
            $fields = array_intersect_key($this->fields, $fields);

            parent::insert($this->tableName(), $fields, $data);
        }
    }

    public function load($post)
    {
        if (isset($post['username']) && isset($post['email']) && isset($post['text'])) {
            return true;
        } else {
            return false;
        }
    }
}