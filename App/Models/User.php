<?php

namespace App\Models;

use PDO;
use \Core\Model;

/**
 * Class User
 * @package App\Models
 */
class User extends Model
{
    /** @var PDO */
    public static $db;

    public $fields = [
        'username', 'password',
    ];

    public function __construct()
    {
        self::$db = static::getDB();
    }

    /**
     * @return string
     */
    public function tableName()
    {
        return "user";
    }

    /**
     * Get all the users as an associative array
     *
     * @return array
     */
    public static function getAll()
    {
        $stmt = self::$db->query('SELECT id, name FROM users');
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param string $userName
     * @param string $password
     * @return int
     */
    public function getUserIdByUserNameAndPass(string $userName, string $password): int
    {
        $user = '\'' . $userName . '\'';
        $pass = '\'' . $password . '\'';

        $stmt = self::$db->query('SELECT id FROM ' . $this->tableName() . ' WHERE `username` = ' . $user . ' AND `password` = ' . $pass);
        $user = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if (!empty($user)) {
            return $user[0]['id'];
        }
    }
}
