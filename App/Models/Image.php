<?php
/**
 * Created by PhpStorm.
 * User: nrjto
 * Date: 07.10.2016
 * Time: 16:30
 */

namespace App\Models;

use PDO;
use Core\Model;

class Image extends Model
{

    /**
     * @return string
     */
    public function tableName()
    {
        return "image";
    }

    /**
     * @param string $name
     * @param string $url
     * @return int
     */
    public function addImageInDB(string $name, string $url): int
    {
        /** @var PDO $db */
        $db = static::getDB();
        $stmt = $db->prepare('INSERT INTO image (name, url) VALUES (:name, :url)');
        $stmt->bindParam(':name', $name);
        $stmt->bindParam(':url', $url);
        $stmt->execute();

        return $db->lastInsertId();
    }

    /**
     * @param int $id
     * @return string
     */
    public function getUrlById(int $id): string
    {
        $db = static::getDB();
        $stmt = $db->query('SELECT url FROM image');
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }


}