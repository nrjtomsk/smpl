<?php
/**
 * Created by PhpStorm.
 * User: nrjto
 * Date: 07.10.2016
 * Time: 21:17
 */

namespace App\Controllers;


use App\Models\Feedback;
use App\Models\Session;
use App\Models\User;
use App\Service\FeedbackService;
use Core\Controller;
use Core\Request;
use Core\Route;
use Core\View;

/**
 * Class AdminController
 * @package App\Controllers
 */
class AdminController extends Controller
{
    public function loginAction()
    {
        $userModel = new User();
        $sessionModel = new Session();
        if (Request::postData()) {
            $data = Request::postData();
            $userId = $userModel->getUserIdByUserNameAndPass($data['username'], md5($data['password']));
            $sessionId = $sessionModel->getNotExpiredSessionByUserId($userId);
            if ($sessionId === null) {
                $sessionId = $sessionModel->createSession($userId);
            }
            Route::redirect('Admin', 'index', $sessionId);
        }
        View::renderTemplate('Admin/login.html');
    }

    public function indexAction($id = null)
    {
        $session = new Session();

        if ($session->getNotExpiredSessionByUserId($id) !== null) {
            $model = new Feedback();
            $data = $model::getAll();

            View::renderTemplate('Admin/index.html', ['data' => FeedbackService::prepareDataView($data)]);
        } else {
            Route::redirect('Admin', 'login');
        }
    }
}