<?php

namespace App\Controllers;

use App\Models\Feedback;
use App\Models\Image;
use App\Service\FeedbackService;
use App\Service\ImageService;
use Core\Request;
use \Core\View;
use \Core\Controller;

/**
 * Class Home
 * @package App\Controllers
 */
class HomeController extends Controller
{
    /** @var  ImageService */
    public $imageService;

    /** @var  Image */
    public $imageModel;

    public $feedbackModel;

    /**
     * HomeController constructor.
     */
    public function __construct()
    {
        $this->imageService = new ImageService();
        $this->imageModel = new Image();
        parent::__construct();
    }


    public function indexAction()
    {
        $model = new Feedback();
        $data = $model::getAll();

        if ($model->load(Request::postData())) {
            $postData = Request::postData();

            if (Request::fileData() !== null) {
                $file = $this->imageService->uploadImage(Request::fileData());
                $imageId = $this->imageModel->addImageInDB($file['name'], $file['path']);
                $postData['image'] = $imageId;
            }

            if ($model->validate()) {
                $model->insert($model->tableName(), $model->fields, FeedbackService::prepareDataDB($postData));
                $this->refresh();
            }
        }

        View::renderTemplate('Home/index.html', ['data' => FeedbackService::prepareDataView($data)]);
    }

    public function sendFeedbackAction()
    {

    }
}
